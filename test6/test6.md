**期末项目设计报告**

| 题  目   | 基于Oracle数据库的商品销售系统的设计 |      |              |
| -------- | ------------------------------------ | ---- | ------------ |
| 课   程  | Oracle数据库应用                     |      |              |
| 学  院   | 计算机学院                           |      |              |
| 专  业   | 软件工程                             | 年级 | 2020级       |
| 学生姓名 | 吴天祺                               | 学号 | 202010414321 |
| 指导教师 | 赵卫东                               | 职称 | 教授         |

 

| ***\*评分项\**** | ***\*评分标准\****           | ***\*满分\**** | ***\*得分\**** |
| ---------------- | ---------------------------- | -------------- | -------------- |
| 文档整体         | 文档内容详实、规范，美观大方 | 10             |                |
| 表设计           | 表，表空间设计合理，数据合理 | 20             |                |
| 用户管理         | 权限及用户分配方案设计正确   | 20             |                |
| PL/SQL设计       | 存储过程和函数设计正确       | 30             |                |
| 备份方案         | 备份方案设计正确             | 20             |                |
| **得分合计**     |                              |                |                |

2023 年  5 月  26 日

 

 

 

# **（一）概要设计**

## **一、绪论**

商品销售系统是一套完整的电商平台软件，它的出现对于企业来说具有重大的意义。首先，商品销售系统能够提高企业的销售效率和管理水平，帮助企业拓展业务和增加销售渠道。其次，该系统提供了丰富的数据分析功能，帮助企业更好地了解市场和客户需求，为企业的决策提供数据支持。最后，商品销售系统能够提高企业的竞争力，帮助企业在电商行业立于不败之地。

## **二、数据库部署模式**

越来越多的人选择去电商购物、庞大商品数据为电影院带来巨大利润的同时,也带来了信息系统风险的相对集中，这使得电商信息系统连续运行的要求也越来越高。加强信息系统灾备体系建设，保障业务连续运行，已经成为影响影院竞争能力的一个重要因素。对RTO=0/RPO=0的系统决定数据库采用RAC+DataDataGuard模式。根据RAC+DataDataGuard模式的特点，有如下要求: .1 主机与备机在物理.上要分开。为了实现容灾的特性，需要在物理。上分割主机和备机。. .2 进行合理 的设计，充分实现DATAGUARD的功能。 注: RTO ( RecoveryTime object): 恢复时间目标，灾难发生后信息系统从停顿到必须恢复的时间要求。 RPO (Recovery Point Object): 恢复点目标，指一个过去的时间点，当灾难或紧急事件发生时，数据可以恢复到的时间点。

## **三、Oracle数据库**

Oracle数据库是美国甲骨文公司（Oracle Corporation）推出的关系型数据库管理系统。它是一种大型的、高可靠性的数据库，通常用于企业级应用。Oracle数据库产品具有许多先进的功能，如分布式事务处理、在线备份、高可用性等。Oracle数据库是世界上使用最广泛的关系型数据库管理系统之一，被财富排行榜上的前1000家公司所采用。



 

 

 

# （二）**详细设计**

1、 表空间设计方案。至少两个表空间

-- 创建主表空间

CREATE TABLESPACE main_ts

DATAFILE 'd:/main_ts.dbf'

SIZE 100M

AUTOEXTEND ON

NEXT 100M

MAXSIZE UNLIMITED;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps1.jpg) 

-- 创建指定表空间

CREATE TABLESPACE specific_ts

DATAFILE 'd:/specific_ts.dbf'

SIZE 50M

AUTOEXTEND ON

NEXT 50M

MAXSIZE UNLIMITED;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps2.jpg) 

2、设计权限及用户分配方案。至少两个用户。

-- 创建系统管理员用户

CREATE USER adminuser IDENTIFIED BY 123  

DEFAULT TABLESPACE system_tablespace;

-- 授权

GRANT CONNECT, RESOURCE, DBA TO adminuser;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps3.jpg) 

 

 

-- 创建客户用户

CREATE USER customeruser IDENTIFIED BY 123

DEFAULT TABLESPACE specific_ts;

-- 授权

GRANT CONNECT, RESOURCE TO customeruser;

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps4.jpg) 

3、创建表

-- member会员信息表

create table members(

Memberid  VARCHAR2(10) not null primary key,

MemberName  VARCHAR2(10),

MemberSex VARCHAR2(2),

MemberBirth date,

MemberPhone VARCHAR2(50)

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps5.jpg) 

-- suppliers供应商信息表

create table suppliers (

SupplierId  VARCHAR2(10) not null primary key,

SupplierName  VARCHAR2(50) not null,

SupplierAddress VARCHAR2(100),

SupplierPhone VARCHAR2(50)

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps6.jpg) 

-- goods商品信息表

create table goods(

GoodId VARCHAR2(10)  not null primary key,

GoodName VARCHAR2(50) not null,

GoodPrice float not null,

GoodType VARCHAR2(10),

GoodStock int,

GoodState VARCHAR2(10),

SupplierId  VARCHAR2(10),

foreign key(SupplierId) references suppliers(SupplierId)

 

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps7.jpg) 

-- orders订单信息表

create table orders(

MemberId  VARCHAR2(10),

GoodId   VARCHAR2(10),

OrderNum  int  not null,

OrderPrice  float  not null,

OrderDate  date,  

OrderStatue  VARCHAR2(20),

primary key(MemberId,GoodId),

foreign key(memberid) references members(memberid),

foreign key(GoodId) references goods(GoodId)

);

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps8.jpg) 

4、在数据库中建立一个程序包

-- 创建程序包

CREATE OR REPLACE PACKAGE sales_pkg IS

 -- 存储过程：插入指定数量的数据到指定表

 PROCEDURE insert_chinese_data(p_table_name IN VARCHAR2, p_num_records IN NUMBER);

 

 -- 存储过程：根据会员ID获取订单总金额

 PROCEDURE get_total_order_amount(p_member_id IN VARCHAR2, p_total_amount OUT NUMBER);

 

 -- 函数：根据会员ID获取会员姓名

 FUNCTION get_member_name(p_member_id IN VARCHAR2) RETURN VARCHAR2;

 

 -- 函数：获取所有供应商数量

 FUNCTION get_supplier_count RETURN NUMBER;

END sales_pkg;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps9.jpg) 

-- 创建程序包体

CREATE OR REPLACE PACKAGE BODY sales_pkg IS

 -- 存储过程：插入指定数量的数据到指定表

 PROCEDURE insert_chinese_data(p_table_name IN VARCHAR2, p_num_records IN NUMBER) IS

  v_counter NUMBER := 1;

 BEGIN

  WHILE v_counter <= p_num_records LOOP

   -- 根据表名插入数据

   IF p_table_name = 'members' THEN

​    INSERT INTO members (Memberid, MemberName, MemberSex, MemberBirth, MemberPhone)

​    VALUES (

​     'M' || v_counter,

​     '会员' || v_counter,

​     CASE WHEN MOD(v_counter, 2) = 0 THEN '男' ELSE '女' END,

​     TO_DATE('1970-01-01', 'YYYY-MM-DD'),

​     '手机号' || v_counter

​    );

   ELSIF p_table_name = 'suppliers' THEN

​    INSERT INTO suppliers (SupplierId, SupplierName, SupplierAddress, SupplierPhone)

​    VALUES (

​     'S' || v_counter,

​     '供应商' || v_counter,

​     '地址' || v_counter,

​     '手机号' || v_counter

​    );

   ELSIF p_table_name = 'goods' THEN

​    INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState, SupplierId)

​    VALUES (

​     'G' || v_counter,

​     '商品' || v_counter,

​     DBMS_RANDOM.VALUE(1, 100),

​     '类型' || v_counter,

​     DBMS_RANDOM.VALUE(0, 100),

​     CASE WHEN MOD(v_counter, 2) = 0 THEN '正常' ELSE '下架' END,

​     'S' || v_counter

​    );

   ELSIF p_table_name = 'orders' THEN

​    INSERT INTO orders (MemberId, GoodId, OrderNum, OrderPrice, OrderDate, OrderStatue)

​    VALUES (

​     'M' || v_counter,

​     'G' || v_counter,

​     DBMS_RANDOM.VALUE(1, 10),

​     DBMS_RANDOM.VALUE(10, 1000),

​     SYSDATE,

​     CASE WHEN MOD(v_counter, 2) = 0 THEN '已支付' ELSE '待支付' END

​    );

   END IF;

   

   v_counter := v_counter + 1;

  END LOOP;

  

  COMMIT;

  DBMS_OUTPUT.PUT_LINE('数据插入完成');

 EXCEPTION

  WHEN OTHERS THEN

   DBMS_OUTPUT.PUT_LINE('数据插入失败: ' || SQLERRM);

   ROLLBACK;

 END insert_chinese_data;

 

 -- 存储过程：根据会员ID获取订单总金额

 PROCEDURE get_total_order_amount(p_member_id IN VARCHAR2, p_total_amount OUT NUMBER) IS

 BEGIN

  SELECT SUM(OrderPrice)

  INTO p_total_amount

  FROM orders

  WHERE MemberId = p_member_id;

 EXCEPTION

  WHEN NO_DATA_FOUND THEN

   p_total_amount := 0;

 END get_total_order_amount;

 

 -- 函数：根据会员ID获取会员姓名

 FUNCTION get_member_name(p_member_id IN VARCHAR2) RETURN VARCHAR2 IS

  v_member_name VARCHAR2(50);

 BEGIN

  SELECT MemberName

  INTO v_member_name

  FROM members

  WHERE Memberid = p_member_id;

  

  RETURN v_member_name;

 EXCEPTION

  WHEN NO_DATA_FOUND THEN

   RETURN NULL;

 END get_member_name;

 

 -- 函数：获取所有供应商数量

 FUNCTION get_supplier_count RETURN NUMBER IS

  v_supplier_count NUMBER;

 BEGIN

  SELECT COUNT(*)

  INTO v_supplier_count

  FROM suppliers;

  

  RETURN v_supplier_count;

 END get_supplier_count;

END sales_pkg;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps10.jpg) 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps11.jpg) 

 

调用包

 

-- 调用 insert_chinese_data 存储过程，向 members 表插入 10 万条数据

BEGIN

 sales_pkg.insert_chinese_data('members', 100000);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps12.jpg) 

-- 调用 insert_chinese_data 存储过程，向 suppliers 表插入 10 万条数据

BEGIN

 sales_pkg.insert_chinese_data('suppliers', 100000);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps13.jpg) 

-- 调用 insert_chinese_data 存储过程，向 goods 表插入 10 万条数据

BEGIN

 sales_pkg.insert_chinese_data('goods', 100000);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps14.jpg) 

-- 调用 insert_chinese_data 存储过程，向 orders 表插入 10 万条数据

BEGIN

 sales_pkg.insert_chinese_data('orders', 100000);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps15.jpg) 

-- 调用 get_total_order_amount 存储过程，计算会员 "M1" 的订单总金额

DECLARE

 v_total_amount NUMBER;

BEGIN

 sales_pkg.get_total_order_amount('M1', v_total_amount);

 DBMS_OUTPUT.PUT_LINE('会员 M1 的订单总金额为: ' || v_total_amount);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps16.jpg) 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps17.jpg) 

-- 调用 get_member_name 函数，获取会员 "M2" 的姓名

DECLARE

 v_member_name VARCHAR2(50);

BEGIN

 v_member_name := sales_pkg.get_member_name('M2');

 DBMS_OUTPUT.PUT_LINE('会员 M2 的姓名为: ' || v_member_name);

END;

/

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps18.jpg) 

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps19.jpg) 

-- 调用 get_supplier_count 函数，获取供应商数量

DECLARE

 v_supplier_count NUMBER;

BEGIN

 v_supplier_count := sales_pkg.get_supplier_count;

 DBMS_OUTPUT.PUT_LINE('供应商数量为: ' || v_supplier_count);

END;

/ ![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps20.jpg)

![img](file:///C:\Users\ADMINI~1\AppData\Local\Temp\ksohtml21156\wps21.jpg) 

5、设计一套数据库的备份方案

1、导出/导入(expdp/impdp 或 exp/imp)

  利用 exp 或 expdp 可将数据从数据库中提取出来，再利用 imp 或 impdp 将提取出来的数据送回到 Oracle 数据库中去。

2、冷备份

  冷备份发生在数据库已经正常关闭的情况下，当正常关闭时数据库是一致性的。对于备份 Oracle 数据库而言，冷备份是最快和最安全的备份方法

  冷备份中必须拷贝的文件包括：所有数据文件、所有控制文件、所有归档重做日志文件、以及初始化参数文件（可选）。冷备必须在数据库关闭的情况下进行，当数据库处于打开状态时，执行数据库文件系统备份是无效的确保在故障发生时，可以应用最新的变更操作，减少数据丢失。
