
-- 创建主表空间
CREATE TABLESPACE main_ts
DATAFILE 'd:/main_ts.dbf'
SIZE 100M
AUTOEXTEND ON
NEXT 100M
MAXSIZE UNLIMITED;

-- 创建指定表空间
CREATE TABLESPACE specific_ts
DATAFILE 'd:/specific_ts.dbf'
SIZE 50M
AUTOEXTEND ON
NEXT 50M
MAXSIZE UNLIMITED;



  
-- 创建系统管理员用户
CREATE USER adminuser IDENTIFIED BY 123   
DEFAULT TABLESPACE system_tablespace;
-- 授权
GRANT CONNECT, RESOURCE, DBA TO adminuser;

-- 创建客户用户
CREATE USER customeruser IDENTIFIED BY 123
DEFAULT TABLESPACE specific_ts;
-- 授权
GRANT CONNECT, RESOURCE TO customeruser;





-- 创建表
-- member会员信息表
create table members(
Memberid  VARCHAR2(10) not null primary key,
MemberName  VARCHAR2(10),
MemberSex VARCHAR2(2),
MemberBirth date,
MemberPhone VARCHAR2(50)
);

-- suppliers供应商信息表
create table suppliers (
SupplierId  VARCHAR2(10) not null primary key,
SupplierName  VARCHAR2(50) not null,
SupplierAddress VARCHAR2(100),
SupplierPhone VARCHAR2(50)
);

-- goods商品信息表
create table goods(
GoodId VARCHAR2(10)  not null primary key,
GoodName VARCHAR2(50) not null,
GoodPrice float not null,
GoodType VARCHAR2(10),
GoodStock int,
GoodState VARCHAR2(10),
SupplierId  VARCHAR2(10),
foreign key(SupplierId) references suppliers(SupplierId)

);


-- orders订单信息表
create table orders(
MemberId  VARCHAR2(10),
GoodId    VARCHAR2(10),
OrderNum  int   not null,
OrderPrice  float  not null,
OrderDate  date,  
OrderStatue  VARCHAR2(20),
primary key(MemberId,GoodId),
foreign key(memberid) references members(memberid),
foreign key(GoodId) references goods(GoodId)
);
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg IS
  -- 存储过程：插入指定数量的数据到指定表
  PROCEDURE insert_chinese_data(p_table_name IN VARCHAR2, p_num_records IN NUMBER);
  
  -- 存储过程：根据会员ID获取订单总金额
  PROCEDURE get_total_order_amount(p_member_id IN VARCHAR2, p_total_amount OUT NUMBER);
  
  -- 函数：根据会员ID获取会员姓名
  FUNCTION get_member_name(p_member_id IN VARCHAR2) RETURN VARCHAR2;
  
  -- 函数：获取所有供应商数量
  FUNCTION get_supplier_count RETURN NUMBER;
END sales_pkg;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  -- 存储过程：插入指定数量的数据到指定表
  PROCEDURE insert_chinese_data(p_table_name IN VARCHAR2, p_num_records IN NUMBER) IS
    v_counter NUMBER := 1;
  BEGIN
    WHILE v_counter <= p_num_records LOOP
      -- 根据表名插入数据
      IF p_table_name = 'members' THEN
        INSERT INTO members (Memberid, MemberName, MemberSex, MemberBirth, MemberPhone)
        VALUES (
          'M' || v_counter,
          '会员' || v_counter,
          CASE WHEN MOD(v_counter, 2) = 0 THEN '男' ELSE '女' END,
          TO_DATE('1970-01-01', 'YYYY-MM-DD'),
          '手机号' || v_counter
        );
      ELSIF p_table_name = 'suppliers' THEN
        INSERT INTO suppliers (SupplierId, SupplierName, SupplierAddress, SupplierPhone)
        VALUES (
          'S' || v_counter,
          '供应商' || v_counter,
          '地址' || v_counter,
          '手机号' || v_counter
        );
      ELSIF p_table_name = 'goods' THEN
        INSERT INTO goods (GoodId, GoodName, GoodPrice, GoodType, GoodStock, GoodState, SupplierId)
        VALUES (
          'G' || v_counter,
          '商品' || v_counter,
          DBMS_RANDOM.VALUE(1, 100),
          '类型' || v_counter,
          DBMS_RANDOM.VALUE(0, 100),
          CASE WHEN MOD(v_counter, 2) = 0 THEN '正常' ELSE '下架' END,
          'S' || v_counter
        );
      ELSIF p_table_name = 'orders' THEN
        INSERT INTO orders (MemberId, GoodId, OrderNum, OrderPrice, OrderDate, OrderStatue)
        VALUES (
          'M' || v_counter,
          'G' || v_counter,
          DBMS_RANDOM.VALUE(1, 10),
          DBMS_RANDOM.VALUE(10, 1000),
          SYSDATE,
          CASE WHEN MOD(v_counter, 2) = 0 THEN '已支付' ELSE '待支付' END
        );
      END IF;
      
      v_counter := v_counter + 1;
    END LOOP;
    
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('数据插入完成');
  EXCEPTION
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('数据插入失败: ' || SQLERRM);
      ROLLBACK;
  END insert_chinese_data;
  
  -- 存储过程：根据会员ID获取订单总金额
  PROCEDURE get_total_order_amount(p_member_id IN VARCHAR2, p_total_amount OUT NUMBER) IS
  BEGIN
    SELECT SUM(OrderPrice)
    INTO p_total_amount
    FROM orders
    WHERE MemberId = p_member_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_total_amount := 0;
  END get_total_order_amount;
  
  -- 函数：根据会员ID获取会员姓名
  FUNCTION get_member_name(p_member_id IN VARCHAR2) RETURN VARCHAR2 IS
    v_member_name VARCHAR2(50);
  BEGIN
    SELECT MemberName
    INTO v_member_name
    FROM members
    WHERE Memberid = p_member_id;
    
    RETURN v_member_name;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NULL;
  END get_member_name;
  
  -- 函数：获取所有供应商数量
  FUNCTION get_supplier_count RETURN NUMBER IS
    v_supplier_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO v_supplier_count
    FROM suppliers;
    
    RETURN v_supplier_count;
  END get_supplier_count;
END sales_pkg;
/

-- 调用 insert_chinese_data 存储过程，向 members 表插入 10 万条数据
BEGIN
  sales_pkg.insert_chinese_data('members', 100000);
END;
/

-- 调用 insert_chinese_data 存储过程，向 suppliers 表插入 10 万条数据
BEGIN
  sales_pkg.insert_chinese_data('suppliers', 100000);
END;
/

-- 调用 insert_chinese_data 存储过程，向 goods 表插入 10 万条数据
BEGIN
  sales_pkg.insert_chinese_data('goods', 100000);
END;
/

-- 调用 insert_chinese_data 存储过程，向 orders 表插入 10 万条数据
BEGIN
  sales_pkg.insert_chinese_data('orders', 100000);
END;
/

-- 调用 get_total_order_amount 存储过程，计算会员 "M1" 的订单总金额
DECLARE
  v_total_amount NUMBER;
BEGIN
  sales_pkg.get_total_order_amount('M1', v_total_amount);
  DBMS_OUTPUT.PUT_LINE('会员 M1 的订单总金额为: ' || v_total_amount);
END;
/

-- 调用 get_member_name 函数，获取会员 "M2" 的姓名
DECLARE
  v_member_name VARCHAR2(50);
BEGIN
  v_member_name := sales_pkg.get_member_name('M2');
  DBMS_OUTPUT.PUT_LINE('会员 M2 的姓名为: ' || v_member_name);
END;
/

-- 调用 get_supplier_count 函数，获取供应商数量
DECLARE
  v_supplier_count NUMBER;
BEGIN
  v_supplier_count := sales_pkg.get_supplier_count;
  DBMS_OUTPUT.PUT_LINE('供应商数量为: ' || v_supplier_count);
END;
/

